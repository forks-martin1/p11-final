# ENTREGA CONVOCATORIA JUNIO

MARTÍN MARTÍNEZ GARCÍA m.martinezga.2021@alumnos.urjc.es

## Parte básica

El funcionamiento de la parte básica esta completo utilizando google Chrome. Utilizando Firefox funciona pero no se visualiza el vídeo.

## Parte adicional

	* Información adicional para los ficheros

	    Se ejecuta exactamente igual que en la parte básica, los streamers mandan un diccionario con clave el titulo del vídeo que muestra y valor la descripción de el mismo.
	  
	* Funcionamiento entre distintas máquinas.

	    Funciona perfectamente con el signaling en una máquina, el front en otra y el streamer en otra. 
	    Primero ejecuto signaling.py pasando el argumento signal.port.
	    Luego ejecuto streamer.py en otra máquina con los argumentos video_file y signal_ip y puerto_ip de la máquina donde ejecutamos el signaling.py. 
	    Por último, ejecuto front.py en otra máquina con los argumentos http.port y signal_ip y puerto_ip de la máquina donde ejecutamos signaling.py.
	    
	    La captura para esta parte se llama capturaMultiplesMaquinas.pcap
	  
## Parte colectiva

	Para esta parte he ejecutado el signaling.py de Hao Zhou, el cual se encuentra en mi carpeta de entrega con el nombre signalingHao.py. Y funciona perfectamente pasando los mismos argumentos que en la parte básica. Los ficheros SDP para este caso se llaman colectivaNavegador.sdp y colectiva_streamer.sdp y la captura capturaColectiva.pcap
	

Enlace del vídeo enseñando como funciona todo: https://www.youtube.com/watch?v=E5mmZHCvQcY



